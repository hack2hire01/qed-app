#!flask/bin/python
from __future__ import print_function # In python 2.7
from flask import Flask, jsonify, request, render_template

#from pymongo import MongoClient
#import pymongo
import datetime
import re
#from apscheduler.schedulers.background import BackgroundScheduler
import time
import os
import random
import sys
import json
from Savoir import Savoir
rpcuser = 'multichainrpc'
rpcpasswd = 'HACJMU7PwSZRCRCy6w3qfjARR6VR9biXaUSk9eG8wgS7'
rpchost='35.154.231.25'
rpcport='7450'
chainname = 'qedChain'
stream = 'qed7'

bcapi = Savoir(rpcuser, rpcpasswd, rpchost, rpcport, chainname)
bcapi.subscribe(stream)

#client = MongoClient('localhost', 27017)
#db1 = client.local
#coll1 = db1.qed

app = Flask(__name__)
masterdata = []



def refresh_masterdata():
	global masterdata
	masterdata_from_bc = bcapi.liststreamitems(stream)
	masterdata= []
	masterdata_filtered = {}
	#return jsonify(masterdata_from_bc)
	for block in masterdata_from_bc:
		curr_key = block['key']
		#print(curr_key)
		if(masterdata_filtered.get(curr_key) == None):
			masterdata_filtered[curr_key] = block
		else:
			print("1:"+str(masterdata_filtered.get(curr_key).get('blocktime')))
			print("2:"+str(block.get('blocktime')))
			#print(json.dumps(block))
			if(block.get('blocktime') == None):
				masterdata_filtered[curr_key] = block
			elif(masterdata_filtered.get(curr_key).get('blocktime') < block.get('blocktime')):
				masterdata_filtered[curr_key] = block
			
	for key,block in masterdata_filtered.items():
		try:
			decoded = json.loads(block['data'].decode("hex"))
			#print(decoded)
			#print(json.loads(decoded))
			masterdata.append(decoded)
		except:
			pass
#facebok api token
#EAACEdEose0cBAIU7g4CKZAlG2TZBaMcMths5QMpiZBsiPDTu5ndXhNbkyUCgrDNw36ZAHGmIRmRWzolixxV24YvbSKj0zptZBKMonoLcPNNQjEwwGqmmgR9BsAWK0phBnwUksablo5C6jysF8ZBUaAeM8HWvqKmKxyZC5VTJbePDzWoNZAeujJLV


@app.route('/viz')
def static_viz_page():
    return render_template('viz.html')


@app.route('/reg')
def static_page():
    return render_template('form-register.jshtml')

@app.route('/submitData')
def submitData():
    return "hit the form"

@app.route('/linked_in_page')
def linked_in_page():
    return render_template('linkedIn.jshtml')

@app.route('/see_details')
def see_details():
    return render_template('browseDetails.html')


@app.route('/')
def index():
    return 'Flask is running!'


@app.route('/data')
def names():
    data = {"names": ["John", "Jacob", "Julie", "Jennifer"]}
    return jsonify(data)



@app.route('/create', methods=['POST'])
#@app.route('/create')
def create():
    try:
        if request.method == "POST":
        #if request.method == "GET":
            print('creating')
            userdata = request.get_json()
            print('creating2')
            print(json.dumps(userdata))
            print('creating3 ',str(userdata['QEDid']))

            userid=str(userdata['QEDid'])
            userid=userid.strip()
            print("user QED id is",userid)
            print('creating4')
            tohex=json.dumps(userdata).encode("hex")
            bcapi.publish(stream,userid,tohex)
            refresh_masterdata()
            print("refreshed")
            return jsonify(success=True,data={})
    except Exception as e:
            print("error!!!")
            print(str(e))
            response=jsonify(success=False,data={})
            return response


def find(paramname, paramval):
    print('looking for ',paramval)    
    return [x for x in masterdata if x[paramname] == paramval]  



@app.route('/list', methods=['GET'])
def list():
	return jsonify(masterdata)


@app.route('/findPost/QEDid/', methods=['POST'])
def findByID():
    QEDid=int(request.form['QEDid'])
    print("Find id ",QEDid)        
    return render_template('displayDetails.html', resultSet=jsonify(find('QEDid',QEDid)))
    #return resultSet
    return jsonify(find('QEDid',QEDid))



@app.route('/get_score/<int:QEDid>/<int:paramval>')
def get_score(QEDid, paramval):
   # req = urllib2.Request('http://localhost:5432/score/' + str(QEDid) + '/' + str(paramval))
   # response = urllib2.urlopen(req)
   # return response.read()
   return jsonify([6.0])



@app.route('/find/QEDid/<int:paramval>', methods=['GET'])
#@app.route('/find/QEDid/<paramval>', methods=['GET'])
def findname(paramval):
    #data=jsonify(find('QEDid',paramval))
    #print(data)    
    return jsonify(find('QEDid',paramval))
    #return data




refresh_masterdata()

if __name__ == '__main__':
    app.run(host='0.0.0.0')



